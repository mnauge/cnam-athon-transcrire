# -*- coding: utf-8 -*-
"""
Created on Mon May 17 09:32:51 2021

@author: Michael Nauge, Poitiers Univeristy
"""

import sys

import scrapOmekaTranscrire as sot



def omkTransPage2StrJpg_test():
    urlTarget = "https://transcrire.huma-num.fr/scripto/8/105/10286"
    
    jpgId = sot.omkTransPage2StrJpg(urlTarget)
    print(jpgId)
    
    

def omkTransPage2StrTrans_test():
    
    urlTarget = "https://transcrire.huma-num.fr/scripto/1/20/554"
    
    print("try to get "+urlTarget)

    try :
        strTrans = sot.omkTransPage2StrTrans(urlTarget)
        
        print(strTrans)
        
    except ValueError:
        print("error omkTransPage2StrTrans")





def omkTransPage2Txt_test():
    urlTarget = "https://transcrire.huma-num.fr/scripto/8/105/10286"
    pathDirTxt = "./../datas/01-primary/"
    
    sot.omkTransPage2Txt(urlTarget, pathDirTxt)


    
    pass
    
    
    
def omkTransCarnet2listUrlPages_test():
    urlTarget = "https://transcrire.huma-num.fr/scripto/10/76/media"
    
    listUrl = omkTransCarnet2listUrlPages(urlTarget)
    pass

    
def main(argv):
    
    omkTransPage2StrJpg_test()
    
    #omkTransPage2StrTrans_test()
    
if __name__ == "__main__":
    main(sys.argv) 