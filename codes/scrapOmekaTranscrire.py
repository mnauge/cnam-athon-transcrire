# -*- coding: utf-8 -*-
"""
Created on Mon May 17 09:14:07 2021

@author: Michael Nauge, Poitiers Univeristy
"""




def omkTransPage2StrTrans(urlTarget):
    """
    Obtenir une string contenant la transcription contenu
    dans la page web ciblée
    

    Parameters
    url : STR
        une url d'une page de document du site transcrire.
        
        exemple: https://transcrire.huma-num.fr/scripto/1/20/554
        

    Raises
    ------
    omekaTranscrireException
        Lève des exceptions si le serveur répond par un message d'erreur.
        
    Returns
    -------
    strTrans : STR
        une str contenant la transcription obtenue
        
    
    """
    
    
    strTrans = "hello world"

    return strTrans


def omkTransPage2StrJpg(urlTarget):
    """
    Obtenir le nom identifier du fichier JPG transcrit de la page cible
    

    Parameters
    ----------
    urlTarget : STR
        une url d'une page de document du site transcrire.
        
        exemple: https://transcrire.huma-num.fr/scripto/1/20/554.

    Returns
    -------
    jpgFileIdentifier : STR

    """
    
    
    strJpgId = "toto.jpg"
    
    pass

    return strJpgId
    

def omkTransPage2Txt(urlTarget, pathDirTxt):
    """
    Générer un fichier TXT (utf-8) contenant la transcription contenu
    dans la page web ciblée. 
    Le nom du fichier TXT doit être celui de l'image JPG.
    
    Cette fonction utilise 
    la fonction omkTransPage2StrTrans(urlTarget) pour obtenir la transcription.
    la fonction omkTransPage2StrJpg(urlTarget) pour obtenir le nom du fichier JPG
    transcrit
    
    

    Parameters
    urlTarget : STR
        une url d'une page de document du site transcrire.
        
        exemple: https://transcrire.huma-num.fr/scripto/1/20/554
        
        
    pathDirTxt : STR
        le chemin du dossier pour recevoir le fichier TXT de sortie

    
    Raises
    ------
    omekaTranscrireException
        Lève des exceptions si le serveur répond par un message d'erreur.
           
    
    """
    
    
    pass



def omkTransCarnet2listUrlPages(urlTargetCarnet):
    
    """
    Générer la liste contenant les liens vers les pages
    contenant des transcriptions pour le carnet précisé par son
    urlTarget.
    
    

    Parameters
    urlTarget : STR
        une url de carnet.
        
        exemple:  "https://transcrire.huma-num.fr/scripto/1/20/media"

        


    
    Raises
    ------
    omekaTranscrireException
        Lève des exceptions si le serveur répond par un message d'erreur.
           
    
    """
    
    pass
    

def omkTransCarnet2Txt(urlTargetCarnet, pathDirTxt):
    """
    Générer tous les fichiers TXT contenant toutes les transcriptions d'un carnet
    
    Cette fonction utilise 
    la fonction omkTransCarnet2listUrlPages(urlTargetCarnet) pour obtenir la liste des url de page à visiter
    la fonction omkTransPage2Txt(urlTargetPage) pour produire le TXT d'une page cible
    
    

    Parameters
    urlTargetCarnet : STR
        une url de carnet du site transcrire.
        
        exemple: https://transcrire.huma-num.fr/scripto/8/122/media
        
        
    pathDirTxt : STR
        le chemin du dossier pour recevoir les fichiers TXT de sortie

    
    Raises
    ------
    omekaTranscrireException
        Lève des exceptions si le serveur répond par un message d'erreur.
           
    
    """
    
    
    pass

    
    

def getDictAuthorTranscript(idPage):
    
    """
    Obtenir une liste de dictionnaire contenant date, auteur, état contenant 
    l'historique de transcription. Passer par omeka S ou le wiki.
    
    

    Parameters
    idPage : STR
        un id de page
        
        exemple:  "8/122/11398"
        urlTarget : "https://transcrire.huma-num.fr/scripto/8/122/11398/revision" 
        ou
        urlTarget : "https://transcrire.huma-num.fr/MEDIAWIKI/index.php?title=8:122:11398&action=history"

        

    Returns
    -------
    listDictHist : LIST[DICT{date,author,state}]
    """
    
    pass


def omkTransCarnet2dfMetas(urlTargetCarnet):
    
    """
    Obtenir une dataframe contenant le plus possible de métadonnées d'un carnet
    
    
    

    Parameters
    urlTargetCarnet : STR
        une url de carnet.
        exemple:  "https://transcrire.huma-num.fr/scripto/8/122/media"

        

    Returns
    -------
    dfMetas : Dataframe 
        Cette dataframe doit contenir le plus possible de métadonnées.
        Pour commencer, il faut les colonnes minimales suivantes : url, idFile
        
    """
    pass
    
    
    
    
    