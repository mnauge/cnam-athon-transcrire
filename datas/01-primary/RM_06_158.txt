153

Fragment de crâne humain (Voir M.C CHAMLA, 1968, p.31)

[dessin] Planorbis [dessin] Melania tuberculata [dessin] [Bullimers?]

Quelques bifaces paléolithiques caractéristiques sur le bâtin à l'est du tarf : 1 biface cheuléen (?) à patine ... assez récente 3 bifaces acheuléens 3 pointes d'éclat paléo M 49.170

Départ 13 H 45. vrai désert de pierres, très dur pour les pieds des chameaux, qui s'en ressentiront par la suite. Je comprends mieux pour quelles raisons l'on connaît si peu les gisements du bâtin et du tarf, etc... les chameliers font tout leur possible pour l'éviter. Agators. 16 H. Caravane [Oulad Billalallanet?] de Néma à Akarijit. 16 h 30. Arrivée au tarf es Sedra. Je note, à l'ouest du tarf, à un km environ au sud de l,extrémité du tarf, une butte témoin que je décide d'aller voir, dans l'espoir que c'est une terrasse lacustre en place.

↑ N approx.

[plan] Chbar (acropole ) paléo scories de fer tarf Es Sedra Arche naturelle Tombe préislam (?) dans enceinte Coquillages paléo moyen Village néolithique Enceinte du village néo Le village se continuait au delà. Dunes Campement 10 [Z? 2?]
