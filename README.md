# CNAM-athon : Transcrire
Michael Nauge, Université de Poitiers (2021)

*Formation aux **basic skills** du métier d'ingénieur DATA en Science Humaine au travers d'un hackathon de la plateforme Transcrire*

## Problématique /  Cahier des charges

> Il me faut une moulinette pour rappatrier le résultat des transcriptions de mon corpus de carnet de terrain actuellement accessible sur le site : [Transcrire](https://transcrire.huma-num.fr). J'en ai besoin pour faire des fouilles de texte et la création d'index spatio-temporelles et nominatifs. J'ai aussi besoin de savoir qui sont les auteurs des transcriptions et l'état d'avancement car je crois que certains collègues n'ont pas tout à fait fini leurs pages.*F. Metada (chercheur en SHS)* 


Si je comprends bien il faut :
* Collecter des données variées dispersées dans une platforme web
* Les re-structurer et les enrichir
* Créer différentes matrices facilitant l'interrogation

C'est ça ?

> Oui, je veux bien des graphiques interactifs aussi avec des widget sympas.

Euh, oui, on verra ce qu'on peut faire dans les temps. C'est pour quand ?

> Hier, évidemment ;-)

Ah, parfait, on est large alors :-)


## Data
*Visite du site [Transcrire](https://transcrire.huma-num.fr)*

### Workflow de projet DH (générique)
<img src="https://gitlab.huma-num.fr/mshs-poitiers/forellis/datagrowth/raw/master/schema_GenericProject.svg">

### Données primaires et dérivées
<img src="https://gitlab.huma-num.fr/mshs-poitiers/forellis/datagrowth/raw/master/schema_Primary-Derived.svg"> 

## Schéma 
<img src="./schema_defis.svg" alt="schema pour se reperer" width="800"/>

## Skill 1 : Web request et parsing
Interroger un entrepôt de données web via l'UIX ou API 

[Notebook Scrap And Parse from UIX](./codes/scrapAndParse.ipynb)

[Notebook Connect to API](https://gitlab.huma-num.fr/mnauge/nakalapyconnect)

**Pas de Jupyter sur sa machine :**
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmnauge%2Fcnam-athon-transcrire/HEAD)  


### Défis Scrap Transcription
Programmer dans le fichier [scrapOmekaTranscrire.py](./codes/scrapOmekaTranscrire.py) les fonctions :
* **omkTransPage2StrTrans(urlTarget)** : Obtenir une string contenant la transcription contenu dans la page web ciblée.

* **omkTransPage2StrJpg(urlTarget)** : Obtenir le nom identifier du fichier JPG transcrit de la page cible.


* **omkTransPage2Txt(urlTarget, pathDirTxt)** : Générer un fichier TXT (utf-8) contenant la transcription contenu dans la page web ciblée. Le nom du fichier TXT doit être celui de l'image JPG. Cette fonction utilise : 
    * la fonction omkTransPage2StrTrans(urlTarget) pour obtenir la transcription. 
    * la fonction omkTransPage2StrJpg(urlTarget) pour obtenir le nom du fichier JPG transcrit


* **omkTransCarnet2Txt(urlTargetCarnet, pathDirTxt)** : Générer tous les fichiers TXT contenant toutes les transcriptions d'un carnet. Cette fonction utilise : 
    * la fonction omkTransCarnet2listUrlPages(urlTargetCarnet) pour obtenir la liste des url de page à visiter
    * la fonction omkTransPage2Txt(urlTargetPage) pour produire le TXT d'une page cible
    
Exploiter l'API de Omeka S : https://omeka.org/s/docs/developer/api/


### Défis Scrap Historique Auteur Transcription (contre la montre)
Programmer dans le fichier [scrapOmekaTranscrire.py](./codes/scrapOmekaTranscrire.py) la fonction :
*  **getDictAuthorTranscript(idPage)** : Obtenir une liste de dictionnaire contenant date, auteur, état contenant l'historique de transcription. Passer par omeka S ou le wiki.
    
    
## Skill 2 : Regex
Construire et écrire des motifs permettant l'extraction de portions de textes.

[Notebook Regex](./codes/regexBasics.ipynb)

## Skill 3 : Natural language processing 
Utiliser une bibliothèque NLP pour l'extraction d'entitées nommées.

Exemple : [Spacy linguistic-feature : named entities](https://spacy.io/usage/linguistic-features#named-entities-101)
```
import spacy

nlp = spacy.load("fr_core_news_sm")
doc = nlp("Tokyo le 9 juillet 1909 Mon cher Monsieur Charles.")

for ent in doc.ents:
    print(ent.text, ent.start_char, ent.end_char, ent.label_)
```    

## Skill 4 : Linear or Human Data to Rich Cleaned Tidy Data


[Notebook Extracteur d'occurences avec Regex](https://bit.ly/2S1ng5i)

[Notebook Pandas on practice on HAL API](https://gitlab.huma-num.fr/mnauge/executable-paper/-/blob/master/notebook/IamInteractivePaper.ipynb)


### Tidy Data
> Wickham, Hadley (20 February 2013). "Tidy Data" ([PDF](https://www.jstatsoft.org/index.php/jss/article/view/v059i10/v59i10.pdf)). Journal of Statistical Software.

> Pierre, Baudin (1 octobre 2020). "Tidy Data, le concept de données propres" ([Blog](https://blog.avanci.fr/tidy-data-le-concept-de-donnees-propres)) Blog Agence Avenci.

### Quality Data
Vérification de la qualité des données [goodTable](https://gitlab.huma-num.fr/mnauge/goodtablesexperiment), Regex le retour !




### Défis Métas Dataframe

Programmer dans le fichier [scrapOmekaTranscrire.py](./codes/scrapOmekaTranscrire.py) la fonction :
* **omkTransCarnet2dfMetas(urlTargetCarnet)** : Cette dataframe doit contenir le plus possible de métadonnées. Pour commencer, il faut les colonnes minimales suivantes : url, idFile. Se référer au fichier [metas.xlsx](./datas/02-dataframe/metas.xlsx). Il sera ensuite possible de sauvegarder la dataframe résultat dans un fichier CSV et/ou XLSX.


### Défis Regex Dataframe
Programmer dans le fichier [txt2df_regex.py](./codes/txt2df_regex.py) la fonction :
* **dirTxt2dfAnnot(pathDirTxt)** : Cette fonction doit parcourir tous les fichiers TXT d'un dossier à la recherche des mots entre crochets. Chaque occurence de mot entre crochet doit produire une nouvelle ligne dans la dataframe resultat. Les colonnes peuvent être : idfile, annot



### Défis NamedEntites Dataframe

Programmer dans le fichier [txt2df_regex.py](./codes/txt2df_regex.py) la fonction :
* **dirTxt2dfnamedEntities(pathDirTxt)** : Cette fonction doit parcourir tous les fichiers TXT d'un dossier à la recherche des entitées nommées idenfiables avec SPACY. Chaque occurence de named entity doit produire une nouvelle ligne dans la dataframe resultat. Les colonnes peuvent être : idfile, type, value


## Skill 5 : Dataviz, interpretabilité des données et communication

Nous n'aurons pas le temps, donc pour le fun, voici un article à lire avec un regards critique (comme toujours !) 

> Le Data Storytelling est un nouveau domaine d’expertise où convergent la science quantitative, cognitive et un autre type de science – celle de la communication [Data Storytelling](https://toucantoco.com/blog/data-storytelling-vs-data-visualization/)









